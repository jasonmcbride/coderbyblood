
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, maximum-scale=1">
	<title>CoderByBlood</title>

	<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<!-- <link href="css/style2.css" rel="stylesheet" type="text/css"> -->
	<link href="css/linecons.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet" type="text/css">
	<link href="css/responsive.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css">
	 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<link href='https://fonts.googleapis.com/css?family=Lato:400,900,700,700italic,400italic,300italic,300,100italic,100,900italic' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Dosis:400,500,700,800,600,300,200' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<script type="text/javascript" src="js/jquery.1.8.3.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/jquery-scrolltofixed.js"></script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="js/jquery.isotope.js"></script>
	<script type="text/javascript" src="js/wow.js"></script>
	<script type="text/javascript" src="js/classie.js"></script>

	<script type="text/javascript">
		$(document).ready(function(e) {
			$('.res-nav_click').click(function() {
				$('ul.toggle').slideToggle(600)
			});

			$(document).ready(function() {
				$(window).bind('scroll', function() {
					if ($(window).scrollTop() > 0) {
						$('#header_outer').addClass('fixed');
					} else {
						$('#header_outer').removeClass('fixed');
					}
				});

			});


		});

		function resizeText() {
			var preferredWidth = 767;
			var displayWidth = window.innerWidth;
			var percentage = displayWidth / preferredWidth;
			var fontsizetitle = 25;
			var newFontSizeTitle = Math.floor(fontsizetitle * percentage);
			$(".divclass").css("font-size", newFontSizeTitle)
		}
	</script>
</head>

<body>

	<!--Header_section-->
	<header id="header_outer" class="scroll-to-fixed-fixed">

		<div class="container">
			<div class="header_section">
				<div class="logo"><img src="https://i.ibb.co/FY3fdp8/Coder-By-Blood-Logo-Banner.png" alt="Coder-By-Blood-Logo-Banner" ></div>


				<nav class="nav" id="nav">
						<ul class="toggle">
						<li><a href="#top_content">Home</a></li>
						<li><a href="#about">About</a></li>
						<li><a href="#products">Products</a></li>
						<li><a href="#team">Team</a></li>
					
					</ul>
				
					<ul class="">
						<li><a href="#top_content">Home</a></li>
						<li><a href="#about">About</a></li>
						<li><a href="#products">Products</a></li>
						<li><a href="#team">Team</a></li>
					
					</ul>
				</nav>
				<a class="res-nav_click animated wobble wow animated" style="visibility: visible; animation-name: wobble;"><i class="fas fa-align-center"></i></a>
			
			 </div>
		</div>
	</header>
			
	<!--Header_section-->

	<!--Top_content-->
	<section id="top_content" class="top_cont_outer">
		<div class="top_cont_inner">
			<div class="container">
				<div class="top_content">
					<div class="row">
						<div class="col-lg-5 col-sm-7">
							<div class="top_left_cont flipInY wow animated">
								<h3 style="color:red">Built for You, by Us</h3>
								<h2>Creating Products and 
                Services to Enhance our Customers Lives  </h2>
								<p>  </p>
								<a href="#about" class="learn_more2" style="background-color:red">Learn more</a> </div>
						</div>
						<div class="col-lg-7 col-sm-5"> </div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--Top_content-->

	<!--Service-->
	<section id="about">
		<div class="container">
			<h2>Our Beginning</h2>
			<div class="service_area">
				<div class="row">
					<div class="col-lg-4">
						<div class="service_block">
					<img src="https://i.ibb.co/gr7RCNy/coderblood.png" alt="coderbyblood" border="0">
							<div class=" delay-03s animated wow  zoomIn"></div>
							<h3 class="animated fadeInUp wow">Coder By Blood </h3>
							<p class="animated fadeInDown wow">Built on the passion of using technology to better the lives of others.</p>
						</div>
					</div>

					<div class="col-lg-4">
						<div class="service_block">


							<img src="https://i.ibb.co/x18cknQ/coderdna.png" alt="coderbyblood" border="0">
							<div class=" delay-03s animated wow zoomIn"> </div>
							<h3 class="animated fadeInUp wow">Architect By Trade</h3>
							<p class="animated fadeInDown wow">We Build. We Create. We Design.</p>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="service_block">

							<img src="https://i.ibb.co/wWykNfB/codercrown.png" alt="coderbyblood" border="0">
							<div class="delay-03s animated wow zoomIn"> </div>
							<h3 class="animated fadeInUp wow">Servant Leader By Choice</h3>
							<p class="animated fadeInDown wow">Always Strive to Support our Team and Customers.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--Service-->



	<section class="main-section alabaster" id="products">
		<!--main-section alabaster-start-->
		<div class="container">
			<div class="row">
			
					
				
				<h2 style="padding-bottom: 50px">Products</h2>
					<figure class="col-lg-5 col-sm-4 wow fadeInLeft">
					<a href="https://bytelyfe.app/app"> <img src="https://i.ibb.co/J3FxdH7/Byte-Lyfe-Logo.png" alt=""></a>
				</figure>
				<div class="col-lg-7 col-sm-8 featured-work">
					<div class="service-list-col2">
									<h3 style="font-size: 25px;padding-bottom: 15px">ByteLyfe</h3>
									<p style="padding-bottom: 25px">Fresh take on daily journaling. Tell your story one byte at a time.</p>
								</div>
		<div class="work_bottom">  <a href="https://bytelyfe.app/app" class="contact_btn">Check it Out!</a> </div>
						</div>
				</div>
			</div>
		</div>
	</section>

	<!--c-logo-part-end-->
	<section class="main-section team" id="team">
		<!--main-section team-start-->
		<div class="container">
			<h2>Our Team</h2>
			<h6>Thank you for making everything possible </h6>
			
				<div class="team-leader-box">
					<div class="team-leader wow fadeInDown delay-03s">
						
						<img src="https://i.ibb.co/61Qgt4n/20100912-154243000-i-OS.png" alt="">
				
					</div>
			
					<h3 class="wow fadeInDown delay-03s">Phillip Smith</h3>
					<span class="wow fadeInDown delay-03s">President</span>
					<p class="wow fadeInDown delay-03s">" I am a coder by blood, an architect by trade, and a leader by choice. I have written code since I was 7 and earned my Masters in Information Technology Management from Carnegie Mellon University. Go Tartans!"</p>
				</div>

				<div class="team-leader-box">
					<div class="team-leader  wow fadeInDown delay-06s">
						
						<img src="https://i.ibb.co/Ht7XwNt/Jerry.png" alt="">

					</div>
					<h3 class="wow fadeInDown delay-06s">Jerry Flowers</h3>
					<span class="wow fadeInDown delay-06s">Vice President</span>
					<p class="wow fadeInDown delay-06s">" I have a background in psychology and education, which I use to ensure products remain intuitive and fun. My goal is to enhance your interactive experience with our products."</p>
				</div>
		
				<div class="team-leader-box">
					<div class="team-leader wow fadeInDown delay-03s">
			
						<img src="https://i.ibb.co/RbsNchg/Rachel.png" alt="">
					</div>
					<h3 class="wow fadeInDown delay-03s">Rachel Bussert</h3>
					<span class="wow fadeInDown delay-03s">Project Manager</span>
					<p class="wow fadeInDown delay-03s">

"I earned my MBA from Western Governors University and have 13 years of experience managing products and people. I am obsessed with data and believe that empathy is the key to excellent customer experiences."

    

</p>
				</div>

					<div class="team-leader-box" >
					<div class="team-leader wow fadeInDown delay-03s">
						
						<img src="https://i.ibb.co/NjDLQDf/Jason.png" alt="">
			
					</div>

					<h3 class="wow fadeInDown delay-03s">Jason McBride</h3>
					<span class="wow fadeInDown delay-03s">Business Development</span>
					<p class="wow fadeInDown delay-03s">"My background is in management and entreprenuership. I have a passion for finding problems and creating solutions. The bigger problems to solve, the better."  </p>
				</div>


	</section>
					
				
			
	<footer class="footer">
		<div class="container">
		
				
				<ul class="social-link" >
				<li class="facebook"><a href="https://www.facebook.com/Coder-By-Blood-Inc-696617307452726"><i class="fa fa-facebook"></i></a></li>
				<li class="instagram"><a href="https://www.instagram.com/coderbyblood/"><i class="fa fa-instagram"></i></a></li>
			
				<li class="twitter"><a href=" https://twitter.com/CoderByBloodInc"><i class="fa fa-twitter"></i></a></li>
			</ul>
		<div class="footer_bottom">
				<span>© CoderByBlood</span>
				</div>
		
		
	</footer>
	<script type="text/javascript">
		$(document).ready(function(e) {
			$('#header_outer').scrollToFixed();
			$('.res-nav_click').click(function() {
				$('.main-nav').slideToggle();
				return false

			});

		});
	</script>
	<script>
		wow = new WOW({
			animateClass: 'animated',
			offset: 100
		});
		wow.init();
		document.getElementById('').onclick = function() {
			var section = document.createElement('section');
			section.className = 'wow fadeInDown';
			section.className = 'wow shake';
			section.className = 'wow zoomIn';
			section.className = 'wow lightSpeedIn';
			this.parentNode.insertBefore(section, this);
		};
	</script>
	<script type="text/javascript">
		$(window).load(function() {

			$('a').bind('click', function(event) {
				var $anchor = $(this);

				$('html, body').stop().animate({
					scrollTop: $($anchor.attr('href')).offset().top - 91
				}, 1500, 'easeInOutExpo');
				/*
				if you don't want to use the easing effects:
				$('html, body').stop().animate({
					scrollTop: $($anchor.attr('href')).offset().top
				}, 1000);
				*/
				event.preventDefault();
			});
		})
	</script>

	<script type="text/javascript">
		jQuery(document).ready(function($) {
			// Portfolio Isotope
			var container = $('#portfolio-wrap');


			container.isotope({
				animationEngine: 'best-available',
				animationOptions: {
					duration: 200,
					queue: false
				},
				layoutMode: 'fitRows'
			});

			$('#filters a').click(function() {
				$('#filters a').removeClass('active');
				$(this).addClass('active');
				var selector = $(this).attr('data-filter');
				container.isotope({
					filter: selector
				});
				setProjects();
				return false;
			});


			function splitColumns() {
				var winWidth = $(window).width(),
					columnNumb = 1;


				if (winWidth > 1024) {
					columnNumb = 4;
				} else if (winWidth > 900) {
					columnNumb = 2;
				} else if (winWidth > 479) {
					columnNumb = 2;
				} else if (winWidth < 479) {
					columnNumb = 1;
				}

				return columnNumb;
			}

			function setColumns() {
				var winWidth = $(window).width(),
					columnNumb = splitColumns(),
					postWidth = Math.floor(winWidth / columnNumb);

				container.find('.portfolio-item').each(function() {
					$(this).css({
						width: postWidth + 'px'
					});
				});
			}

			function setProjects() {
				setColumns();
				container.isotope('reLayout');
			}

			container.imagesLoaded(function() {
				setColumns();
			});


			$(window).bind('resize', function() {
				setProjects();
			});

		});
		$(window).load(function() {
			jQuery('#all').click();
			return false;
		});
	</script>


</body>
</html>
